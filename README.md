# Zoom

This role installs Zoom from Flatpak.
You might want to consider locking down its app permissions using
[Flatseal](https://github.com/tchx84/Flatseal)
or [similar](https://www.mayrhofer.eu.org/post/zoom-flatpak-sandboxing/).


## Zoom in Firejail?

That is why I originally created the firejail role, but I never found the time
to figure out the necessary configuration.

I [recently learnt](https://github.com/alexjung/Run-Zoom-in-a-Sandbox) that
there is now (an unofficial) [Flatpak for Zoom](https://flathub.org/apps/details/us.zoom.Zoom).
This might represent the best of both worlds: no configuration needed,
and yet sandboxing.

```
flatpak install flathub us.zoom.Zoom
flatpak run us.zoom.Zoom
```

+ https://ar.al/2020/06/25/how-to-use-the-zoom-malware-safely-on-linux-if-you-absolutely-have-to/


## Manual installation

Notes from my first, manual installation. Worked without issues with the Zoom-provided DEB-file.

```
taha@x230t:~/Downloads
wget https://zoom.us/client/latest/zoom_amd64.deb
$ sudo apt install ./zoom_amd64.deb
Reading package lists... Done
Building dependency tree
Reading state information... Done
Note, selecting 'zoom' instead of './zoom_amd64.deb'
The following additional packages will be installed:
  dconf-cli gir1.2-ibus-1.0 ibus im-config libibus-1.0-5
  libxcb-xtest0
Suggested packages:
  ibus-clutter ibus-doc ibus-qt4
The following NEW packages will be installed:
  dconf-cli gir1.2-ibus-1.0 ibus im-config libibus-1.0-5
  libxcb-xtest0 zoom
0 upgraded, 7 newly installed, 0 to remove and 0 not upgraded.
Need to get 4,805 kB/77.5 MB of archives.
After this operation, 314 MB of additional disk space will be used.
Do you want to continue? [Y/n]
```


## Links

+ [Latest Zoom client for Ubuntu 14.04+ 64-bit](https://zoom.us/client/latest/zoom_amd64.deb)
+ [Zoom public key](https://zoom.us/linux/download/pubkey)
  with fingerprint
  3960 60CA DD8A 7522 0BFC B369 B903 BF18 61A7 C71D
+ [Zoom has a good page with instructions on installing on various Linux distros](https://support.zoom.us/hc/en-us/articles/204206269-Installing-Zoom-on-Linux)
+ [Zoom's download page](https://zoom.us/download?os=linux)
+ [Zoom installers](https://support.zoom.us/hc/en-us/articles/207373866-Zoom-Installers)
+ [Installing Zoom on Linux](https://support.zoom.us/hc/en-us/articles/204206269-Installing-or-updating-Zoom-on-Linux)
+ [Zoom's release notes for Linux](https://support.zoom.us/hc/en-us/articles/205759689-New-Updates-for-Linux)
+ [Useful tips on how to launch Zoom from the command line](https://waynewerner.com/blog/launch-zoom-from-the-command-line.html)
+ https://reddit.com/r/linux4noobs/comments/qitrnm/using_zoom_on_linux/
+ https://aur.archlinux.org/packages/zoom-firejail
+ https://github.com/netblue30/firejail/blob/master/etc/profile-m-z/zoom.profile
