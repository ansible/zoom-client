# Flatpak silently fails to start on Jammy

Even running from terminal produced no output. Had to use `-v` flag:

```
taha@asks2:~
$ flatpak run -v us.zoom.Zoom
F: No installations directory in /etc/flatpak/installations.d. Skipping
F: Opening system flatpak installation at path /var/lib/flatpak
F: Opening user flatpak installation at path /home/taha/.local/share/flatpak
F: Opening user flatpak installation at path /home/taha/.local/share/flatpak
F: Skipping parental controls check for app/us.zoom.Zoom/x86_64/stable since parental controls are disabled globally
F: Opening user flatpak installation at path /home/taha/.local/share/flatpak
F: /home/taha/.local/share/flatpak/runtime/org.freedesktop.Platform/x86_64/21.08/b76cb66319a7c5877ea02dcdd4d8bd53b3c6470b6f9c96fb3238ff216bf991c7/files/lib32 does not exist
F: Cleaning up unused container id 1623202492
F: Cleaning up per-app-ID state for us.zoom.Zoom
F: Cleaning up unused container id 1561059387
F: Cleaning up per-app-ID state for us.zoom.Zoom
F: Allocated instance id 3131395427
F: Add defaults in dir /us/zoom/Zoom/
F: Add locks in dir /us/zoom/Zoom/
F: Allowing wayland access
F: Allowing x11 access
F: Allowing pulseaudio access
F: Pulseaudio user configuration file '/home/taha/.config/pulse/client.conf': Error opening file /home/taha/.config/pulse/client.conf: No such file or directory
F: Running 'bwrap --args 39 xdg-dbus-proxy --args=41'
F: Running 'bwrap --args 39 zoom'
```

+ https://reddit.com/r/voidlinux/comments/frcqv5/issues_with_flatpak_and_pulseaudio/

Huh, my user is not part of the `audio` group. It probably should be.
```
$ id taha
uid=1000(taha) gid=1000(taha) groups=1000(taha),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),110(lxd)
```

Reinstalled Zoom flatpak, and now it starts!
```
$ flatpak run -v us.zoom.Zoom
F: No installations directory in /etc/flatpak/installations.d. Skipping
F: Opening system flatpak installation at path /var/lib/flatpak
F: Opening user flatpak installation at path /home/taha/.local/share/flatpak
F: Opening user flatpak installation at path /home/taha/.local/share/flatpak
F: Skipping parental controls check for app/us.zoom.Zoom/x86_64/stable since parental controls are disabled globally
F: Opening user flatpak installation at path /home/taha/.local/share/flatpak
F: /home/taha/.local/share/flatpak/runtime/org.freedesktop.Platform/x86_64/21.08/c7252386179c4c1ecf5d93bdaec6ca82852dddfdae7112b5c3177f7424c4a928/files/lib32 does not exist
F: Cleaning up unused container id 3946351316
F: Cleaning up per-app-ID state for us.zoom.Zoom
F: Cleaning up unused container id 2096756855
F: Cleaning up per-app-ID state for us.zoom.Zoom
F: Allocated instance id 3764665371
F: Add defaults in dir /us/zoom/Zoom/
F: Add locks in dir /us/zoom/Zoom/
F: Allowing wayland access
F: Allowing x11 access
F: Allowing pulseaudio access
F: Pulseaudio user configuration file '/home/taha/.config/pulse/client.conf': Error opening file /home/taha/.config/pulse/client.conf: No such file or directory
F: Running 'bwrap --args 40 xdg-dbus-proxy --args=42'
F: Running 'bwrap --args 40 zoom'
```
