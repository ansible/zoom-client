## Schedule meeting window is completely blank

+ https://github.com/flathub/us.zoom.Zoom/issues/364#issuecomment-1399625811

This can be fixed by setting `ZYPAK_ZYGOTE_STRATEGY_SPAWN=0`, as suggested
by @ntninja (link above).

This can be achieved by modifying the `Exec=` line:
```
Exec=env ZYPAK_ZYGOTE_STRATEGY_SPAWN=0 /usr/bin/flatpak run --branch=stable --arch=x86_64 --command=zoom --file-forwarding us.zoom.Zoom @@u %U @@
```
which this role does from now on.


## Zoom silently fails to start after updating it

We have apparently upgraded to Zoom 5.16.6.382 (stable).
Running the launcher and nothing happens, not even in the terminal:
```
dex .local/share/flatpak/app/us.zoom.Zoom/current/active/export/share/applications/us.zoom.Zoom.desktop
```

Tailing `~/.zoom/logs/zoom_stdout_stderr.log` shows:
```
ZoomLauncher started.
Zoom path is: /app/extra/zoom
cmd line: 
Start subprocess: /app/extra/zoom/zoom sucessfully,  process pid: 3 
Can't load/home/taha/.config/zoomus.conf
sh: line 1: pacmd: command not found
[13 preload-host-spawn-strategy] Warning: waitpid override ignores groups
[13 preload-host-spawn-strategy] Warning: waitpid override ignores groups
[13 preload-host-spawn-strategy] Warning: waitpid override ignores groups
                             Class      App      Lib Possible Culprit Flags
                resip::Connection      656      656 
                      resip::Data       36       36 
                 resip::DnsResult     1080     1080 
                   resip::Headers        1        1 
          resip::MsgHeaderScanner       40       40 
                resip::SipMessage     5224     5224 
         resip::TransportSelector      896      896 
                     resip::Tuple      128      128 
              resip::UdpTransport     1144     1144 
          resip::GenericIPAddress       28       28 

zoom started.
sh: line 1: pacmd: command not found
                             Class      App      Lib Possible Culprit Flags
                resip::Connection      656      656 
                      resip::Data       36       36 
                 resip::DnsResult     1080     1080 
                   resip::Headers        1        1 
          resip::MsgHeaderScanner       40       40 
                resip::SipMessage     5224     5224 
         resip::TransportSelector      896      896 
                     resip::Tuple      128      128 
              resip::UdpTransport     1144     1144 
          resip::GenericIPAddress       28       28 

zoom started.
sh: line 1: pacmd: command not found
                             Class      App      Lib Possible Culprit Flags
                resip::Connection      656      656 
                      resip::Data       36       36 
                 resip::DnsResult     1080     1080 
                   resip::Headers        1        1 
          resip::MsgHeaderScanner       40       40 
                resip::SipMessage     5224     5224 
         resip::TransportSelector      896      896 
                     resip::Tuple      128      128 
              resip::UdpTransport     1144     1144 
          resip::GenericIPAddress       28       28 

zoom started.
[1124/003336.158437:ERROR:bus.cc(399)] Failed to connect to the bus: Failed to connect to socket /run/dbus/system_bus_socket: No such file or directory
Client: Breakpad is using Single Client Mode! client fd = -1
libva error: vaGetDriverNameByIndex() failed with unknown libva error, driver_name = (null)
sh: line 1: pacmd: command not found
                             Class      App      Lib Possible Culprit Flags
                resip::Connection      656      656 
                      resip::Data       36       36 
                 resip::DnsResult     1080     1080 
                   resip::Headers        1        1 
          resip::MsgHeaderScanner       40       40 
                resip::SipMessage     5224     5224 
         resip::TransportSelector      896      896 
                     resip::Tuple      128      128 
              resip::UdpTransport     1144     1144 
          resip::GenericIPAddress       28       28 

zoom started.
[CZPClientLogMgr::LogClientEnvironment] [MacAddr: 70:85:C2:B6:07:F0][client: Linux][OS:  Freedesktop.org SDK 22.08 Flatpak runtime x64][Hardware: CPU Core:4 Frenquency:3.8748 G Memory size:32016MB CPU Brand:AMD Ryzen 5 2400G with Radeon Vega Graphics     GPU Brand:][Req ID: ]
Linux Client Version is 5.16.6 (382)
QSG_RENDER_LOOP is 
XDG_CURRENT_DESKTOP = i3;   GDMSESSION = i3;   XDG_SESSION_TYPE = x11
Graphics Card Info:: 10:00.0 VGA compatible controller: NVIDIA Corporation GP107GL [Quadro P620] (rev a1)
Zoom package arch is 64bit, runing OS arch is x86_64, snap package 0
qt.glx: qglx_findConfig: Failed to finding matching FBConfig for QSurfaceFormat(version 2.0, options QFlags<QSurfaceFormat::FormatOption>(), depthBufferSize -1, redBufferSize 1, greenBufferSize 1, blueBufferSize 1, alphaBufferSize -1, stencilBufferSize -1, samples -1, swapBehavior QSurfaceFormat::SingleBuffer, swapInterval 1, colorSpace QSurfaceFormat::DefaultColorSpace, profile  QSurfaceFormat::NoProfile)
qt.glx: qglx_findConfig: Failed to finding matching FBConfig for QSurfaceFormat(version 2.0, options QFlags<QSurfaceFormat::FormatOption>(), depthBufferSize -1, redBufferSize 1, greenBufferSize 1, blueBufferSize 1, alphaBufferSize -1, stencilBufferSize -1, samples -1, swapBehavior QSurfaceFormat::SingleBuffer, swapInterval 1, colorSpace QSurfaceFormat::DefaultColorSpace, profile  QSurfaceFormat::NoProfile)
qt.glx: qglx_findConfig: Failed to finding matching FBConfig for QSurfaceFormat(version 2.0, options QFlags<QSurfaceFormat::FormatOption>(), depthBufferSize -1, redBufferSize 1, greenBufferSize 1, blueBufferSize 1, alphaBufferSize -1, stencilBufferSize -1, samples -1, swapBehavior QSurfaceFormat::SingleBuffer, swapInterval 1, colorSpace QSurfaceFormat::DefaultColorSpace, profile  QSurfaceFormat::NoProfile)
Could not initialize GLX
zoom was exited due to a handled signal: 6 
ZoomLauncher exit.
```

`pacmd`?
```
$ apt-cache search pacmd
pulseaudio-utils - Command line tools for the PulseAudio sound server
```
Except `pulseaudio-utils` is already installed:
```
$ dpkg -l | grep pulseaudio-
ii  pulseaudio-module-bluetooth  1:15.99.1+dfsg1-1ubuntu2.1  amd64  Bluetooth module for PulseAudio sound server
ii  pulseaudio-utils             1:15.99.1+dfsg1-1ubuntu2.1  amd64  Command line tools for the PulseAudio sound server
```

What else?
This [comment by dhollinger](https://github.com/flathub/us.zoom.Zoom/issues/404#issuecomment-1753210255):

> anytime your system gets GFX driver updates, you have to run `flatpak update` to
> get the latest GFX framework updates before Zoom will start working again. I run into
> this everytime there's a driver update.

Ok, let's try that:
```
taha@asks2:~
$ flatpak update
Looking for updates…
Info: org.gnome.Platform//43 is end-of-life, with reason:
   The GNOME 43 runtime is no longer supported as of September 20, 2023. Please ask your application developer to migrate to a supported platform.
Applications using this runtime:
   org.remmina.Remmina
Info: org.gnome.Platform//41 is end-of-life, with reason:
   The GNOME 41 runtime is no longer supported as of September 17, 2022. Please ask your application developer to migrate to a supported platform.
Applications using this runtime:
   com.github.needleandthread.vocal
Info: org.gnome.Platform//40 is end-of-life, with reason:
   The GNOME 40 runtime is no longer supported as of March 21, 2022. Please ask your application developer to migrate to a supported platform.
Applications using this runtime:
   com.github.alecaddd.sequeler
Info: org.freedesktop.Platform//20.08 is end-of-life, with reason:
   org.freedesktop.Platform 20.08 is no longer receiving fixes and security updates. Please update to a supported runtime version.
Applications using this runtime:
   com.github.robertsanseries.ciano
Info: org.freedesktop.Platform.GL.default//21.08 is end-of-life, with reason:
   org.freedesktop.Platform 21.08 is no longer receiving fixes and security updates. Please update to a supported runtime version.
Info: org.freedesktop.Platform.GL.default//20.08 is end-of-life, with reason:
   org.freedesktop.Platform 20.08 is no longer receiving fixes and security updates. Please update to a supported runtime version.


        ID                                                      Branch          Op          Remote           Download
 1.     org.freedesktop.Platform.GL.nvidia-525-147-05           1.4             i           flathub          < 415.0 MB

Proceed with these changes to the user installation? [Y/n]: Y
```

That worked! Now the darn program starts.

Since all flatpak updates are handled from our `desktop-tools` role, I have added
a handler that runs `flatpak update` to that role (no updates are done here).



## Small issue with login (SSO) and launch

On SSO login in the client, the client opens a browser window (Chrome on `x230t`) which then shows a big "Launch Zoom" button upon completed authentication. Clicking this button fails to achieve anything (just opens a new, empty browser window).

Right-clicking on this button reveals the command it's trying to run. It looks like this:

```
xdg-open "zoommtg://uu-se.zoom.us/sso?token=<token>"
```

Running this command directly from the terminal works fine, though.

NOTE: this issue works fine if Firefox handles the login.



## Constantly being logged out

But only on `asks2` so far?
I have purged zoom, wiped `~/.zoom`, and reinstalled, but the behaviour persists.

Also (perhaps this is related) when I login using SSO from the desktop client, 
the URL that opens in Firefox is malformed (`https://uu-se/saml/...` instead of `https://uu-se.zoom.us/saml/...`).
Also this behaviour is not evident on `rosetta`.

It seems this problem might be related to manner of startup.
Starting using the regular Zoom desktop entry appears to preserve logged in state.
But starting using my `zoomurl` script consistently logs me out.

Are Zoom doing some magic in the zoom launcher that we fail to replicate when calling
zoom with the `--url` argument?

+ https://devforum.zoom.us/t/stop-logging-zoom-users-out-of-their-desktop-app/17526/10


## Startup error on workstation (nvidia drivers problem, solved)

Zoom failed to start on asks2 today. No error messages, even when executing `zoom` from the terminal.
So I uninstalled (purged) *and* wiped the `~/.zoom/` directory. After reinstalling Zoom (using this role)
I got the following error message:

```
taha@asks2:~
$ zoom 
ZoomLauncher started.
Zoom path is: /opt/zoom
cmd line: 
Start subprocess: /opt/zoom/zoom sucessfully,  process pid: 23376 
zoom started.
Client: Breakpad is using Single Client Mode! client fd = -1
[CZPClientLogMgr::LogClientEnvironment] [MacAddr: 70:85:C2:B6:07:F0][client: Linux][OS:  Ubuntu 18.04.5 LTS x64][Hardware: CPU Core:4 Frenquency:2.862 G Memory size:16003MB CPU Brand:AMD Ryzen 5 2400G with Radeon Vega Graphics     GPU Brand:][Req ID: ]
Linux Client Version is 5.7.1 (26030.0627)
QSG_RENDER_LOOP is 
XDG_CURRENT_DESKTOP = i3;   GDMSESSION = i3;   XDG_SESSION_TYPE = x11
Graphics Card Info:: 10:00.0 VGA compatible controller: NVIDIA Corporation GP107GL [Quadro P620] (rev a1)
Zoom package arch is 64bit, runing OS arch is x86_64
AppIconMgr::systemDesktopName log Desktop Name: i3 
QGLXContext: Failed to create dummy context
qt.scenegraph.general: QSG: basic render loop
qt.scenegraph.general: Using sg animation driver
Failed to create OpenGL context for format QSurfaceFormat(version 2.0, options QFlags<QSurfaceFormat::FormatOption>(), depthBufferSize 24, redBufferSize -1, greenBufferSize -1, blueBufferSize -1, alphaBufferSize 8, stencilBufferSize 8, samples -1, swapBehavior QSurfaceFormat::DoubleBuffer, swapInterval 1, colorSpace QSurfaceFormat::DefaultColorSpace, profile  QSurfaceFormat::NoProfile) 
zoom was exited due to a handled signal: 6 
ZoomLauncher exit.
```

Turns out the nvidia graphics driver was broken.
Error was resolved after purging and reinstalling the nvidia drivers and kernel modules
(more notes in role `graphics-driver-nvidia`).
