# Encryption and Zoom

Logged in to Zoom with Uppsala university's SSO.
In Settings, click on the link **View More Settings**.
This takes you a [webpage](https://uu-se.zoom.us/profile/setting?mid=&from=client)
with quite a lot of settings:

![](files/more-settings-screenshot.png)

I changed the end-to-end encryption from **enhanced** to **E2E**.
